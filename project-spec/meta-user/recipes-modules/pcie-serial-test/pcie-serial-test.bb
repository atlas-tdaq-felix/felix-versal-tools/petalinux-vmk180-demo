SUMMARY = "Recipe for  build an external example-module Linux kernel module"
SECTION = "PETALINUX/modules"
LICENSE = "CLOSED"

inherit module

INHIBIT_PACKAGE_STRIP = "1"

SRC_URI = "file://Makefile \
           file://flx_net.h \
           file://fifo_ops.h \
           file://flx_net_dev.c \
           file://flx_net_target.c \
          "

S = "${WORKDIR}"

# The inherit of module.bbclass will automatically name module packages with
# "kernel-module-" prefix as required by the oe-core build environment.
