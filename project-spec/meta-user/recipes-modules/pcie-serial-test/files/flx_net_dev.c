/*

	This is a demonstration for establishing a network 
	between VMK180 and host PC over CPM. This is the common
	network interface device driver. 

	Elena Zhivun <elena.zhivun@cern.ch>

*/

#include <linux/kernel.h>
#include <linux/vmalloc.h>
#include <linux/errno.h>
#include <linux/io.h>
#include <linux/device.h>
#include <linux/timer.h>
#include <linux/types.h>
#include <linux/ethtool.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/kthread.h>
#include <linux/module.h>
#include <linux/version.h>
#include <linux/circ_buf.h>

#ifdef CONFIG_ZYNQMP_FIRMWARE
#include <linux/firmware/xlnx-zynqmp.h>
#endif

#include "fifo_ops.h"
#include "flx_net.h"

#define TX_TIMEOUT_MS 6
#define RX_TIMEOUT_MS 3
#define POLL_PERIOD_MS 1
#define SLEEP_PERIOD_MS 1000

#define DRV_NAME	"bnl_flxnet"

/*
	TODO:
	- add watchdogs in firmware for "link detect" emulation
		- disable tx and rx on the peers without "link" present
	- it's possible to make TX queue circular buffer lockless
	- user interrupts instead of timer; use NAPI

	BUGs:
	- first ICMP packet takes 1-2 seconds to arrive?
*/


struct net_device * flxnet;
struct mutex flxnet_mutex;
u32 message_level;

typedef u32 t_fifo_word;
// MAX_PACKET_LEN is measured in FIFO words
#define MAX_PACKET_LEN 400
#define BYTES_IN_WORD 4
#define MAX_DATA_LEN MAX_PACKET_LEN * BYTES_IN_WORD
#define MAX_RETRY_ATTEMPTS 5


struct tx_packet {
	int	num_words;
	int attempts;
	char dest_mac[ETH_ALEN];
	char data[MAX_DATA_LEN];
};


int tx_queue_size = 256;
DEFINE_SPINLOCK(tx_queue_spinlock);
struct tx_queue {
	int head;
	int tail;
	struct tx_packet *packets;
};


struct tx_queue tx_queue;

static LIST_HEAD(peers);
struct mutex peer_mutex;
atomic_t peer_count;

wait_queue_head_t recv_queue;
struct timer_list recv_timer;
struct task_struct *recv_task;


#define FLXNET_SOP 0xCACE0000
#define FLXNET_SOP_MASK 0xFFFF0000
#define FLXNET_LEN_MASK 0x0000FFFF
#define FLXNET_EOP 0xFA18CECA

#define RX_SUCCESS 0
#define RX_LOW_BUFFER 1
#define RX_INVALID_ARG 2
#define RX_TIMEOUT 3
#define RX_INVALID_EOP 4
#define RX_NO_MEMORY 5
#define RX_TRUNCATED 6

#define TX_SUCCESS 0
#define TX_TRY_BROADCAST 1
#define TX_BUSY 2
#define TX_ERROR 3

/* Read/Write access to the MMAPped registers */
#if defined(CONFIG_ARCH_ZYNQ) || defined(CONFIG_X86)
# define register_read(offset)		readl(offset)
# define register_write(offset, val)	writel(val, offset)
#else
# define register_read(offset)		__raw_readl(offset)
# define register_write(offset, val)	__raw_writel(val, offset)
#endif

static void tx_all_packets(void);

static inline bool check_flag(void* __iomem reg, int bit_id) {
	return ((1 << bit_id) & register_read(reg)) != 0;
}

static inline bool is_word_available (struct flxnet_peer *peer) {
	return check_flag(peer->regs + peer->reg_status, WORD_AVAILABLE);
}

static inline bool is_block_available (struct flxnet_peer *peer) {
	return check_flag(peer->regs + peer->reg_status, BLOCK_AVAILABLE);
}

static inline bool can_accept_word (struct flxnet_peer *peer) {
	return check_flag(peer->regs + peer->reg_status, HAS_ROOM_FOR_WORD);
}

static inline bool can_accept_block (struct flxnet_peer *peer) {
	return check_flag(peer->regs + peer->reg_status, HAS_ROOM_FOR_BLOCK);
}

// Send a single word to the device
static inline void send_word(struct flxnet_peer* peer, u32 data) {
	register_write(peer->regs + peer->reg_send, data);
}

// Receive a single word
static inline t_fifo_word recv_word(struct flxnet_peer* peer) {
	return (t_fifo_word)register_read(peer->regs + peer->reg_recv);
}

// Verify that the device magic number is correct
inline bool is_magic_correct(struct flxnet_peer* peer) {
	return (register_read(peer->regs + MAGIC_REG) == FLX_DEV_MAGIC);
}

/* 
	Removes the peer from the network, assumes
	that peer_mutex is held
*/
void flxnet_remove_peer(struct flxnet_peer *peer) {
	int rv;

	if (message_level & NETIF_MSG_LINK)
		pr_info("flxnet: removing peer %p from the network", peer);

	rv = atomic_dec_return(&peer_count);
	if (rv < 0) {
		pr_err("BUG in %s: peer_count = %d, less than 0\n",
			__FUNCTION__, rv);
		atomic_set(&peer_count, 0);
	}

	if (peer->reserved) module_put(peer->owner);
	list_del(&peer->list_head);
	kfree(peer);
}


// Removes all peers from the network
void remove_all_peers(void) {
	struct flxnet_peer *peer;
	struct flxnet_peer *tmp;

	if (message_level & NETIF_MSG_LINK)
		pr_info("flxnet: removing all peers from the network");

	mutex_lock(&peer_mutex);
	list_for_each_entry_safe(peer, tmp, &peers, list_head) {
		flxnet_remove_peer(peer);
	}
	mutex_unlock(&peer_mutex);
}


// mark as not reserved so that peer modules can be removed
void flxnet_release_all_peers(void) {
	struct flxnet_peer *peer;
	struct flxnet_peer *tmp;
	
	mutex_lock(&peer_mutex);
	list_for_each_entry_safe(peer, tmp, &peers, list_head) {
		if (peer->reserved) {
			module_put(peer->owner);
			peer->reserved = false;
		} else {
			if (message_level & NETIF_MSG_DRV)
				pr_info("flxnet: trying to release peer %p, which is not reserved, skipping\n", peer);
		}
	}
	mutex_unlock(&peer_mutex);
}


// mark as reserved so that peer modules cannot be removed and iomem stay there
void flxnet_reserve_all_peers(void) {
	struct flxnet_peer *peer;
	struct flxnet_peer *tmp;

	mutex_lock(&peer_mutex);
	list_for_each_entry_safe(peer, tmp, &peers, list_head) {
		if (!peer->reserved) {
			peer->reserved = try_module_get(peer->owner);
			if (!peer->reserved) {
				pr_err("flxnet: failed to reserve peer %p, owner module is missing\n", peer);
				flxnet_remove_peer(peer);
			}
		} else {
			if (message_level & NETIF_MSG_DRV)
				pr_info("flxnet: trying to reserve peer %p, which is already reserved, skipping\n", peer);
		}
	}
	mutex_unlock(&peer_mutex);
}


static bool tx_queue_is_full(void) {
	bool rv;
	unsigned long flags;
	spin_lock_irqsave(&tx_queue_spinlock, flags);
	rv = CIRC_SPACE(tx_queue.head, tx_queue.tail, tx_queue_size) == 0;
	spin_unlock_irqrestore(&tx_queue_spinlock, flags);
	return rv;
} 


static bool tx_queue_is_empty(void) {
	bool rv;
	unsigned long flags;
	spin_lock_irqsave(&tx_queue_spinlock, flags);
	rv = CIRC_CNT(tx_queue.head, tx_queue.tail, tx_queue_size) == 0;
	spin_unlock_irqrestore(&tx_queue_spinlock, flags);
	return rv;
}

	
static int tx_queue_try_push(struct sk_buff *skb) {
	struct tx_packet *packet;
	struct ethhdr *mac_hdr;
	unsigned long flags;

	if (tx_queue_is_full()) {
		if (message_level & NETIF_MSG_TX_ERR)
			pr_info("flxnet: trying to add a packet to TX queue, but it is full\n");
		return TX_BUSY;
	}

	if (skb->len > MAX_DATA_LEN) {
		pr_err("flxnet: (BUG) TX packet payload is too large "
			"(%d bytes, maximum is %d)\n", skb->len, MAX_DATA_LEN);
		return TX_ERROR;
	}

	mac_hdr = eth_hdr(skb);

	spin_lock_irqsave(&tx_queue_spinlock, flags);
	packet = &tx_queue.packets[tx_queue.head];
	packet->num_words = skb->len / BYTES_IN_WORD;
	memcpy(packet->data, skb->data, skb->len);
	if (skb->len % BYTES_IN_WORD) {
		packet->num_words += 1;
		memset(packet->data + skb->len, 0, BYTES_IN_WORD - (skb->len % BYTES_IN_WORD));
	}
	memcpy(packet->dest_mac, mac_hdr->h_dest, ETH_ALEN);
	packet->attempts = 0;
	tx_queue.head = (tx_queue.head + 1) & (tx_queue_size - 1);
	spin_unlock_irqrestore(&tx_queue_spinlock, flags);

	return TX_SUCCESS;
}


static int tx_queue_pop(void) {
	unsigned long flags;

	if (tx_queue_is_empty()) {
		pr_warn("flxnet: trying to remove a packet from TX queue, but it is empty\n");
		return -1;
	}

	spin_lock_irqsave(&tx_queue_spinlock, flags);
	tx_queue.tail = (tx_queue.tail + 1) & (tx_queue_size - 1);
	spin_unlock_irqrestore(&tx_queue_spinlock, flags);
	return 0;
}


void tx_queue_reset(void) {
	unsigned long flags;
	flxnet->stats.tx_dropped += CIRC_CNT(tx_queue.head, tx_queue.tail, tx_queue_size);
	spin_lock_irqsave(&tx_queue_spinlock, flags);
	tx_queue.head = 0;
	tx_queue.tail = 0;
	spin_unlock_irqrestore(&tx_queue_spinlock, flags);	
}


static struct tx_packet * tx_queue_get_next(void) {
	struct tx_packet * result;
	unsigned long flags;

	if (tx_queue_is_empty())
		return NULL;

	spin_lock_irqsave(&tx_queue_spinlock, flags);
	result = &tx_queue.packets[tx_queue.tail];
	spin_unlock_irqrestore(&tx_queue_spinlock, flags);
	return result;
}


// waits for EOP, returns 0 if it is received correctly, -1 otherwise
bool is_valid_eop(struct flxnet_peer *peer, unsigned long timeout) {
	t_fifo_word eop;

	eop = 0;
	while (true) {
		if (is_word_available(peer)) {
			eop = recv_word(peer);
			break;
		}

		if (jiffies > timeout) {
			break;
		}
	}

	if (jiffies > timeout || eop != FLXNET_EOP) {
		return false;
	}

	return true;
}


// check the mac address of the received packet
// and update the peer mac if valid
void update_mac(struct flxnet_peer *peer, struct sk_buff *skb) {
	struct ethhdr *mac_hdr;

	mac_hdr = eth_hdr(skb);
	if (!is_valid_ether_addr(mac_hdr->h_source))
		return;

	if (memcmp(peer->mac_addr, mac_hdr->h_source, ETH_ALEN) != 0) {
		if (message_level & NETIF_MSG_DRV) {
			pr_info("flxnet: updating peer mac address: was %pM, new %pM\n",
				peer->mac_addr, mac_hdr->h_source);
		}
		memcpy(peer->mac_addr, mac_hdr->h_source, ETH_ALEN);
	}	
}


// there is no memory to create skb, so deque the packet
// and drop it
int drop_rx_packet(struct flxnet_peer *peer, int length_words) {
	int num_words;
	unsigned long timeout;

	if (length_words == 0) {
		if (message_level & NETIF_MSG_DRV)
			pr_err("flxnet: attempting to drop 0-length packet\n");
		flxnet->stats.rx_frame_errors += 1;
		return RX_INVALID_ARG;
	}

	timeout = jiffies + msecs_to_jiffies(RX_TIMEOUT_MS);

	num_words = 0;
	while (true) {
		while (is_word_available(peer)) {
			recv_word(peer);
			num_words += 1;
			if (num_words == length_words) {
				goto packet_rx_done;
			}
		}

		if (jiffies > timeout) {
			goto packet_rx_done;
		}
	}

packet_rx_done:

	flxnet->stats.rx_bytes += num_words * BYTES_IN_WORD;

	if (jiffies > timeout) {
		return RX_TIMEOUT;	
	}

	if (num_words != length_words) {
		return RX_TRUNCATED;
	}

	if (!is_valid_eop(peer, timeout)) {
		flxnet->stats.rx_frame_errors += 1;
		return RX_INVALID_EOP;
	}
	
	return RX_NO_MEMORY;
}


int recv_packet(struct flxnet_peer *peer, int length_words) {
	struct sk_buff *skb;	
	int num_words;
	t_fifo_word rx_word;
	unsigned long timeout;
	bool low_buffer;	

	if (length_words == 0 || length_words > MAX_PACKET_LEN) {
		if (message_level & NETIF_MSG_DRV)
			pr_err("flxnet: invalid packet length (%d words)\n",
				length_words);
		flxnet->stats.rx_frame_errors += 1;
		return RX_INVALID_ARG;
	}	

	skb = netdev_alloc_skb(flxnet, length_words * BYTES_IN_WORD);
	if (!skb) {
		flxnet->stats.rx_dropped++;
		if (message_level & NETIF_MSG_RX_ERR)
			pr_err("flxnet: dropped RX packet, low memory\n");
		drop_rx_packet(peer, length_words);
		return RX_NO_MEMORY;
	}

	// socket buffer is available, receive the packet
	timeout = jiffies + msecs_to_jiffies(RX_TIMEOUT_MS);

	num_words = 0;
	low_buffer = false;
	if (message_level & NETIF_MSG_PKTDATA)
		pr_info("flxnet: received packet data:\n");

	while (true) {
		while (is_word_available(peer)) {
			rx_word = recv_word(peer);
			memcpy(skb_put(skb, BYTES_IN_WORD), &rx_word, BYTES_IN_WORD);
			num_words += 1;
			if (message_level & NETIF_MSG_PKTDATA)
				pr_info("%08X\n", rx_word);
			if (num_words == length_words)
				goto packet_rx_done;
		}

		low_buffer = true;

		if (jiffies > timeout) {
			goto packet_rx_done;
		}
	}

packet_rx_done:

	flxnet->stats.rx_bytes += num_words * BYTES_IN_WORD;

	if (jiffies > timeout) {
		dev_kfree_skb(skb);
		flxnet->stats.rx_errors++;
		if (message_level & NETIF_MSG_RX_ERR)
			pr_err("flxnet: error receiving packet, timeout\n");
		return RX_TIMEOUT;
	}

	if (num_words != length_words) {
		dev_kfree_skb(skb);
		flxnet->stats.rx_errors++;
		if (message_level & NETIF_MSG_RX_ERR)
			pr_err_ratelimited("flxnet: error receiving packet, missing data\n");
		return RX_TRUNCATED;
	}

	if (!is_valid_eop(peer, timeout)) {
		dev_kfree_skb(skb);
		flxnet->stats.rx_errors++;
		if (message_level & NETIF_MSG_RX_ERR)
			pr_err_ratelimited("flxnet: received a packet with invalid trailer\n");
		flxnet->stats.rx_frame_errors += 1;
		return RX_INVALID_EOP;
	}
	
	skb->protocol = eth_type_trans(skb, flxnet);
	skb->ip_summed = CHECKSUM_UNNECESSARY;
	flxnet->stats.rx_packets += 1;	
	update_mac(peer, skb);

	if (message_level & NETIF_MSG_RX_STATUS)
		pr_info("flxnet: received packet (%d bytes, %d words) from peer %p\n",
			skb->len, length_words, peer);

	netif_rx_ni(skb);

	if (low_buffer)
		return RX_LOW_BUFFER;
	else
		return RX_SUCCESS;
}

/*
	Timer to wake up FIFO polling thread periodically
*/
void recv_timer_function(struct timer_list *timer_list) {
	wake_up_interruptible(&recv_queue);	
	mod_timer(timer_list, jiffies + msecs_to_jiffies(POLL_PERIOD_MS));
}


// returns true if the packet can be received by at least one peer
bool can_transmit_packets(void) {
	struct flxnet_peer *peer;

	// if there are no peers, packets can be maked as "sent" and 
	// dropped immediatelly
	if (atomic_read(&peer_count) == 0)
		return true;

	mutex_lock(&peer_mutex);
	list_for_each_entry(peer, &peers, list_head) {
		if (!peer->reserved) continue;
		if (is_magic_correct(peer) && can_accept_block(peer)) {
			mutex_unlock(&peer_mutex);
			return true;
		}
	}
	mutex_unlock(&peer_mutex);
	return false;
}


/*
	Iterate over all peers, retrieve all packets under the assumption that
	CPU can receive them faster than FIFO fills up
*/
void rx_all_packets(void) {	
	struct flxnet_peer *peer;
	struct flxnet_peer *tmp;
	int length_words, rv;
	t_fifo_word sop;

	mutex_lock(&peer_mutex);
	list_for_each_entry_safe(peer, tmp, &peers, list_head) {
		if (!peer->reserved) continue;	

		if (!is_magic_correct(peer)) {
			pr_err("flxnet: invalid magic in %s, removing peer %p from the network\n",
				__FUNCTION__, peer);
			flxnet_remove_peer(peer);
			continue;
		}

		while (is_block_available(peer)) {
			if (message_level & NETIF_MSG_RX_STATUS)
				pr_err("flxnet: peer %p has data block available\n", peer);

			sop = recv_word(peer);
			if ((sop & FLXNET_SOP_MASK) != FLXNET_SOP) {
				if (message_level & NETIF_MSG_RX_ERR)
					pr_err("flxnet: invalid SOP: %#010X\n", sop);
				flxnet->stats.rx_frame_errors += 1;
				continue;
			}

			// found the beginning of a packet
			length_words = sop & FLXNET_LEN_MASK;
			rv = recv_packet(peer, length_words); 				
			if (rv == RX_TIMEOUT || rv == RX_LOW_BUFFER) {
				// receiving this packet took too long
				// move on to the next peer
				if (message_level & NETIF_MSG_RX_STATUS)
					pr_err("flxnet: low buffer on peer %p, abort reading\n", peer);
				break;
			} else if (rv == RX_NO_MEMORY) {
				mutex_unlock(&peer_mutex);
				pr_err("flxnet: RX thread is out of memory\n");
				return;
			}
		}
	}	
	mutex_unlock(&peer_mutex);
}


/*
	Background task for polling the FIFOs and reading out packets
	I don't want these in NAPI poll because this can be slow and
	hence potentially interfere with other network devices
*/
int send_recv_thread(void *data) {
	DEFINE_WAIT(wait);

	if (message_level & NETIF_MSG_DRV)
		pr_info("flxnet: entered FIFO polling thread\n");

	while (!kthread_should_stop()) {
		if (tx_queue_is_empty()) {
			prepare_to_wait(&recv_queue, &wait, TASK_INTERRUPTIBLE);
			schedule();
			finish_wait(&recv_queue, &wait);
		}		

		// if (message_level & NETIF_MSG_INTR)
		// 	pr_info("flxnet: wake up worker thread %s\n", __FUNCTION__);

		tx_all_packets();

		if (kthread_should_stop())
			break;

		if (atomic_read(&peer_count) == 0)
			continue;		

		// restart TX queue if it's stalled
		if (!netif_running(flxnet) && can_transmit_packets()) {
			if (message_level & NETIF_MSG_DRV)
				pr_info("flxnet: wake up netif queue (%s)\n", __FUNCTION__);
			netif_wake_queue(flxnet);
		}

		rx_all_packets();
	}

	if (message_level & NETIF_MSG_IFDOWN)
		pr_info("flxnet: stopping %s\n", __FUNCTION__);
	return 0;
}


// add a felix device to the network
struct flxnet_peer * flxnet_add_peer(void* __iomem base_address, int role,
	struct module *owner) {

	struct flxnet_peer *peer;

	peer = kzalloc(sizeof(struct flxnet_peer), GFP_KERNEL);
	if (IS_ERR(peer)) {
		pr_err("flxnet: can't allocate memory for struct flxnet_peer\n");
		return NULL;
	}

	peer->regs = base_address;
	peer->owner = owner;	
	peer->reserved = false;
	INIT_LIST_HEAD(&peer->list_head);

	if (role == ROLE_HOST) {
		peer->reg_send = HOST_ADDR_SEND;
		peer->reg_recv = HOST_ADDR_RECV;
		peer->reg_status = HOST_ADDR_STATUS;
	} else if (role == ROLE_TARGET) {
		peer->reg_send = TARGET_ADDR_SEND;
		peer->reg_recv = TARGET_ADDR_RECV;
		peer->reg_status = TARGET_ADDR_STATUS;
	} else {
		pr_err("flxnet: unknown device role: %d\n", role);
		goto err_invalid_role;
	}

	mutex_lock(&peer_mutex);	
	peer->reserved = try_module_get(owner);
	if (!peer->reserved) {
		mutex_unlock(&peer_mutex);
		goto err_module_get;
	}
	list_add(&peers, &peer->list_head);
	atomic_inc(&peer_count);
	mutex_unlock(&peer_mutex);

	if (message_level & NETIF_MSG_LINK)
		pr_info("flxnet: added peer %p to the network\n", peer);

	return peer;

err_invalid_role:
err_module_get:
	kfree(peer);
	return NULL;
}

int open(struct net_device *dev) {
	mutex_lock(&flxnet_mutex);

	flxnet_reserve_all_peers(); // reserve the modules that supplied the peers	
	tx_queue_reset(); // clear TX queue

	// enable RX side
	if (message_level & NETIF_MSG_IFUP)
		pr_info("flxnet: initialize RX thread and timer\n");
	recv_task = kthread_create(send_recv_thread, NULL, "flxnet RX thread");
	if (IS_ERR(recv_task)) {
		mutex_unlock(&flxnet_mutex);
		pr_err("flxnet: failed to create RX thread in %s\n", __FUNCTION__);
		return -1;
	}
	wake_up_process(recv_task);
	mod_timer(&recv_timer, jiffies + msecs_to_jiffies(POLL_PERIOD_MS));

	// enable TX side
	if (message_level & NETIF_MSG_IFUP)
		pr_info("flxnet: start netif queue\n");
	netif_start_queue(dev);

	mutex_unlock(&flxnet_mutex);
	return 0;
}

int stop(struct net_device *dev) {
	mutex_lock(&flxnet_mutex);

	// disable RX side	
	if (message_level & NETIF_MSG_IFDOWN)
		pr_info("flxnet: stop RX thread\n");	
	wake_up_interruptible(&recv_queue);
	kthread_stop(recv_task);
	del_timer_sync(&recv_timer);

	// disable TX side
	if (message_level & NETIF_MSG_IFDOWN)
		pr_info("flxnet: stop netif queue\n");
	netif_stop_queue(dev);

	tx_queue_reset(); // clear TX queue
	flxnet_release_all_peers(); // free the modules that supplied the peers

	mutex_unlock(&flxnet_mutex);
	
	return 0;
}


static inline void drop_this_packet(void) {
	tx_queue_pop();
	flxnet->stats.tx_dropped += 1;
}


int transmit_packet(struct flxnet_peer *peer, struct tx_packet *packet) {

	int num_words, i;
	t_fifo_word data;

	if (!is_magic_correct(peer)) {
		pr_err("flxnet: invalid magic in %s, removing peer %p from the network,"
			" and dropping the packet\n", __FUNCTION__, peer);
		flxnet_remove_peer(peer);		
		flxnet->stats.tx_dropped += 1;
		return TX_ERROR;
	}

	if (!can_accept_block(peer))
		return TX_BUSY;

	num_words = packet->num_words;

	if (message_level & NETIF_MSG_PKTDATA)
		pr_info("Transmitting data:\n");
	send_word(peer, FLXNET_SOP | num_words);
	for (i = 0; i < num_words; i++) {
		memcpy(&data, &packet->data[i * BYTES_IN_WORD], BYTES_IN_WORD);
		send_word(peer, data);
		if (message_level & NETIF_MSG_PKTDATA)
			pr_info("%08x\n", data);
	}
	send_word(peer, FLXNET_EOP);

	if (message_level & NETIF_MSG_TX_DONE)
		pr_info("flxnet: sent packet (%d words) to peer %p\n", num_words, peer);

	flxnet->stats.tx_packets += 1;
	flxnet->stats.tx_bytes += num_words * BYTES_IN_WORD;
	return TX_SUCCESS;
}


int send_broadcast(struct tx_packet *packet) {
	int rv;	
	struct flxnet_peer *peer, *tmp;

	rv = TX_BUSY;
	mutex_lock(&peer_mutex);
	list_for_each_entry_safe(peer, tmp, &peers, list_head) {
		if (!peer->reserved) continue;

		if (transmit_packet(peer, packet) != TX_BUSY)			
			rv = TX_SUCCESS;
	}
	mutex_unlock(&peer_mutex);

	if (rv == TX_SUCCESS) tx_queue_pop();

	if ((rv == TX_BUSY) && (message_level & NETIF_MSG_TX_ERR)) {
		pr_info("flxnet: no peer has space in FIFO (total %d peers)\n",
			atomic_read(&peer_count));
	}

	return rv;
}


// send out the latest buffered packet 
int send_packet(struct tx_packet *packet) {
	int num_peers, rv;
	struct flxnet_peer *peer, *tmp;

	num_peers = atomic_read(&peer_count);
	if (num_peers == 0) {
		drop_this_packet();
		if (message_level & NETIF_MSG_TX_ERR)
			pr_info("flxnet: discard packet - no peers available\n");
		return TX_SUCCESS;
	}

	packet->attempts += 1;
	if (packet->attempts > MAX_RETRY_ATTEMPTS) {
		drop_this_packet();
		if (message_level & NETIF_MSG_TX_ERR)
			pr_info("flxnet: packet %p is dropped after %d attempts\n",
				packet, packet->attempts);
		return TX_ERROR;
	}

	if (num_peers == 1 || is_multicast_ether_addr(packet->dest_mac))
		return send_broadcast(packet);

	mutex_lock(&peer_mutex);
	list_for_each_entry_safe(peer, tmp, &peers, list_head) {
		if (!peer->reserved) continue;

		if (memcmp(peer->mac_addr, packet->dest_mac, ETH_ALEN) == 0) {
			// matching peer is found in the table
			rv = transmit_packet(peer, packet);
			mutex_unlock(&peer_mutex);
			if (rv != TX_BUSY) tx_queue_pop();
			return rv;
		}
	}
	mutex_unlock(&peer_mutex);

	// matching peer was not found on the list
	return send_broadcast(packet);
}


static void tx_all_packets(void) {
	struct tx_packet *packet;

	while (true) {
		packet = tx_queue_get_next();
		if (packet == NULL) break;
		if (send_packet(packet) == TX_BUSY) break;
	}
}


int hard_start_xmit(struct sk_buff *skb, struct net_device *dev) {

	netif_trans_update(dev);
	if (tx_queue_try_push(skb) == TX_BUSY) {
		if (message_level & NETIF_MSG_TX_ERR)
			pr_info("TX packet queue is full\n");
		netif_stop_queue(flxnet);
		return NETDEV_TX_BUSY;
	}
	
	dev_kfree_skb(skb);
	return NETDEV_TX_OK;
}

#if LINUX_VERSION_CODE >= KERNEL_VERSION(5,6,0)
static void tx_timeout (struct net_device *dev, unsigned int txqueue)
#else
static void tx_timeout (struct net_device *dev)
#endif
{
	flxnet->stats.tx_errors++;
	flxnet->stats.tx_dropped++;

	netif_trans_update(dev);
	if (message_level & NETIF_MSG_DRV)
		pr_info("flxnet: wake up netif queue (%s)\n", __FUNCTION__);

	netif_wake_queue(dev);
}

static struct net_device_stats * get_stats(struct net_device *dev)
{
	return &dev->stats;
}

static void flx_get_drvinfo(struct net_device *dev, struct ethtool_drvinfo *info)
{
	strlcpy(info->driver, DRV_NAME, sizeof(info->driver));
}

static int flx_get_link_ksettings(struct net_device *dev,
				  struct ethtool_link_ksettings *cmd)
{
	cmd->base.speed = SPEED_10;
	cmd->base.duplex = DUPLEX_FULL;
	cmd->base.port = PORT_OTHER;
	return 0;
}

static int flx_set_link_ksettings(struct net_device *dev,
				  const struct ethtool_link_ksettings *cmd)
{
	return -EINVAL;
}

static u32 flx_get_link(struct net_device *dev)
{
	return (u32)(atomic_read(&peer_count) != 0);
}

static u32 flx_get_msglevel(struct net_device *dev)
{
	return message_level;
}

static void flx_set_msglevel(struct net_device *dev, u32 v)
{
	message_level = v;
}


static const struct ethtool_ops ethtool_ops = {
	.get_drvinfo = flx_get_drvinfo,
	.get_link = flx_get_link,
	.get_msglevel = flx_get_msglevel,
	.set_msglevel = flx_set_msglevel,
	.get_link_ksettings = flx_get_link_ksettings,
	.set_link_ksettings = flx_set_link_ksettings,
};


static const struct net_device_ops flx_netdev_ops = {
	.ndo_open            = open,
	.ndo_stop            = stop,
	.ndo_start_xmit      = hard_start_xmit,
	.ndo_get_stats       = get_stats,
	.ndo_tx_timeout 	= tx_timeout,
	.ndo_set_mac_address 	= eth_mac_addr,
	.ndo_validate_addr	= eth_validate_addr,
};

#if IS_REACHABLE(CONFIG_ZYNQMP_FIRMWARE)
union zynq_hw_addr {
	char hw_addr[ETH_ALEN];
	struct {
		u32 version;
		u32 idcode;
	};
};

union zynq_hw_addr zynq_data;
#endif

// Register the network device
int __init flx_ndev_init(void) {
	int rv;

	mutex_init(&peer_mutex);
	mutex_init(&flxnet_mutex);
	atomic_set(&peer_count, 0);	
	message_level = 0;

	// allocate ring buffer for outgoing packets
	tx_queue.packets = kcalloc(tx_queue_size, sizeof(struct tx_packet), GFP_KERNEL);
	if (IS_ERR(tx_queue.packets)) {
		pr_err("flxnet: can't allocate memory for the TX packet queue\n");
		rv = -ENODEV;
		goto err_alloc_tx_queue;
	}
	tx_queue.head = 0;
	tx_queue.tail = 0;

	flxnet = alloc_etherdev(0);
	if (IS_ERR(flxnet)) {
		pr_err("flxnet: can't allocate memory for the network device structure\n");
		rv = -ENODEV;
		goto err_alloc_etherdev;
	}

	strlcpy(flxnet->name, "flxnet%d", IFNAMSIZ);
	flxnet->netdev_ops = &flx_netdev_ops;
	flxnet->ethtool_ops = &ethtool_ops;
	flxnet->watchdog_timeo = msecs_to_jiffies(TX_TIMEOUT_MS);
	flxnet->tx_queue_len = 100;
	flxnet->features = NETIF_F_HW_CSUM;

	init_waitqueue_head(&recv_queue);
	timer_setup(&recv_timer, recv_timer_function, 0);


#if IS_REACHABLE(CONFIG_ZYNQMP_FIRMWARE)
/*
	On Zynq platform, try to assign initial MAC address based on IDCODE 
	and version of the device
*/
	rv = zynqmp_pm_get_chipid(&zynq_data.idcode, &zynq_data.version);
	if (rv < 0) {
		pr_warn("flxnet: failed to get Zynq chip ID, assigning random mac\n");
		eth_random_addr(flxnet->dev_addr);
	} else {
		zynq_data.hw_addr[0] &= 0xfe;	/* clear multicast bit */
		zynq_data.hw_addr[0] |= 0x02;	/* set local assignment bit (IEEE802) */
		//eth_hw_addr_set(flxnet->dev_addr, &zynq);
		memcpy(flxnet->dev_addr, &zynq_data, flxnet->addr_len);
	}

#else
// assign random MAC on other platforms
	eth_random_addr(flxnet->dev_addr);
#endif

	rv = register_netdev(flxnet);
	if (rv) {
		pr_err("flxnet: can't register the network interface");
		goto err_register;
	}

	pr_info("flxnet: registered network interface with MAC %pM\n", flxnet->dev_addr);

	return 0;

err_register:
	free_netdev(flxnet);

err_alloc_etherdev:
	kfree(tx_queue.packets);

err_alloc_tx_queue:
	return rv;
}


// Unegister the network device
void __exit flx_ndev_exit(void) {	
	unregister_netdev(flxnet);
	free_netdev(flxnet);
	remove_all_peers();	
	kfree(tx_queue.packets);
}


MODULE_AUTHOR("Elena Zhivun <elena.zhivun@cern.ch>");
MODULE_DESCRIPTION("Network device for communicating with VMK180 over CPM");

module_init(flx_ndev_init);
module_exit(flx_ndev_exit);

EXPORT_SYMBOL(flxnet_add_peer);
//EXPORT_SYMBOL(flxnet_remove_peer);

MODULE_LICENSE("GPL");
