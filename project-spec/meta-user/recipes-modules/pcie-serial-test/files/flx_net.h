#ifndef FLX_NET_H
#define FLX_NET_H

#include <linux/etherdevice.h>

struct flxnet_peer {
	struct list_head list_head;
	void __iomem *regs;
	unsigned int reg_send;
	unsigned int reg_recv;
	unsigned int reg_status;
	unsigned char mac_addr[ETH_ALEN];
	bool reserved;
	struct module *owner;
};

#define ROLE_HOST 0
#define ROLE_TARGET 1

#define MIN_MMAP_SIZE 0x20

#endif