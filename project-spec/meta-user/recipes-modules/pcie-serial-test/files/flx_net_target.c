/*

	This is an minimal example driver for vmk180 side
	to demonstrate serial communication over PCIe.

	Device tree entry example:

	&amba {
	    pcie-serial@20100050000 {
	        #address-cells = <0x02>;
	        #size-cells = <0x02>;
	        reg = <0x201 0x00050000 0x00 0x100>;        
	        compatible = "bnl,vmk180-serial-example";
	        status = "okay";
	    };
	};

	Elena Zhivun <elena.zhivun@cern.ch>

*/

#include <linux/kernel.h>
#include <linux/moduleparam.h>
#include <linux/stat.h>
#include <linux/platform_device.h>
#include <linux/mm.h>
#include <linux/io.h>
#include <linux/errno.h>
#include <linux/sched.h>
#include <linux/vmalloc.h>
#include <linux/module.h>
#include <linux/version.h>

#include <linux/of_address.h>
#include <linux/of_device.h>
#include <linux/of_platform.h>

#include "flx_net.h"

#define DRV_MODULE_NAME	"flxnet_target"

extern struct flxnet_peer * flxnet_add_peer(void* __iomem base_address,
	int role, struct module *owner);

static int flx_net_probe(struct platform_device *pdev)
{
	void * __iomem base;
	struct flxnet_peer *peer;
#if LINUX_VERSION_CODE < KERNEL_VERSION(5,1,0)
	struct resource *mem;
#endif

#if LINUX_VERSION_CODE >= KERNEL_VERSION(5,1,0)
	base = devm_platform_ioremap_resource(pdev, 0);
#else
	mem = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!mem) {
		dev_err(&pdev->dev, "failed to get memory resource\n");
		return -ENODEV;
	}

	base = devm_ioremap_resource(&pdev->dev, mem);
#endif
	if (IS_ERR(base)) {
		dev_err(&pdev->dev, "failed to ioremap memory resource\n");
		return -ENODEV;
	}

	// add this device to the network
	dev_info(&pdev->dev, "Registering a peer with flxnet_dev\n");
	peer = flxnet_add_peer(base, ROLE_TARGET, THIS_MODULE);
	if (IS_ERR(peer)) {
		dev_err(&pdev->dev, "failed to insert flxnet peer\n");
		return -ENODEV;
	}

	platform_set_drvdata(pdev, peer);
	return 0;
}


static int flx_net_remove(struct platform_device *pdev)
{
	return 0;
}


static const struct of_device_id flx_serial_of_match[] = {
	{ .compatible = "bnl,vmk180-serial-example", .data = NULL },
	{ /* end of table */ }
};
MODULE_DEVICE_TABLE(of, flx_serial_of_match);


static struct platform_driver flx_net_driver = {
	.driver	= {
		.name = DRV_MODULE_NAME,
		.owner = THIS_MODULE,
		.of_match_table = flx_serial_of_match,
	},
	.probe = flx_net_probe,
	.remove = flx_net_remove,
};

module_platform_driver(flx_net_driver);

MODULE_AUTHOR("Elena Zhivun <elena.zhivun@cern.ch>");
MODULE_DESCRIPTION("Example driver establishing network connection with VMK180 over PCIe");
MODULE_LICENSE("GPL");

