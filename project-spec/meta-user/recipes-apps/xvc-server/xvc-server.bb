#
# This file is the xvcServer recipe.
#

SUMMARY = "Xilinx Virtual Cable"
SECTION = "PETALINUX/apps"
LICENSE = "CLOSED"

SRC_URI = "file://xvcserver.c \
	   file://xvcserver.h \
	   file://xvc_mem.c \
	   file://Makefile \
		  "

S = "${WORKDIR}"

do_compile() {
	     oe_runmake
}

do_install() {
	     install -d ${D}${bindir}
	     install -m 0755 bin/xvc_mem ${D}${bindir}
}
