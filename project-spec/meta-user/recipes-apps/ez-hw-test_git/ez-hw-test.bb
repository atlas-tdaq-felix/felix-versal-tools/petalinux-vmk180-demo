SUMMARY = "Web application for automated hardware testing"
DESCRIPTION = "ez-hw-test is a Flask-based web application developed \
for testing custom hardware platforms for the Omega Group in \
Brookhaven National Laboratory, as part of the FELIX phase2 upgrade"
LICENSE = "CLOSED"

RDEPENDS_${PN} = " \
        python3-flask \
        python3-flask-restful \
        python3-flask-bootstrap \
        python3-flask-wtf \
        python3-wtforms \
        python3-werkzeug \
        python3-jinja2 \
        python3-markupsafe \
        python3-itsdangerous \
        python3-twisted \
        python3-gevent \
        python3-matplotlib \
        python3-pillow \
        python3-periphery \        
        iperf3 \
        memtester \
        mtd-utils \
"

SRC_URI = "git://git@gitlab.cern.ch:7999/BNL-ATLAS/tdaq-phase2/flx180/flx-181-webapp.git;protocol=ssh"
SRCREV = "${AUTOREV}"
PV = "1.0+git${SRCPV}"

S = "${WORKDIR}/git"

do_install() {
        install -d ${D}${bindir}/ez-hw-test
        cp -r --no-preserve=ownership ${S}/* -t ${D}${bindir}/ez-hw-test
}

FILES_${PN} += "${bindir}/ez-hw-test/*"