SUMMARY = "FLX-181 demo application"
DESCRIPTION = "Utility packages to help testing the Versal-based \
devices developed for FELIX phase2 upgrade"

inherit packagegroup

FLX181_PACKAGES = " \
        ethtool \
	phytool \
	netcat \
        i2c-tools \
        fio \
        lmbench \        
        iperf3 \
        socat \
        memtester \
        libgpiod \
        libgpiod-tools \
        packagegroup-petalinux-lmsensors \
        lrzsz \
"
RDEPENDS_${PN} = "${FLX181_PACKAGES}"